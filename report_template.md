---
author: group member names (without IDs please)
title: Paper Title
subtitle: Coursework Group NN
institute: 7COM1085 -- Research Methods, University of Hertfordshire
date: 14 May 2021
...


# Introduction

The _Introduction_ section gives the _context_ in which your research question should be
interpreted,  your _research question_, and
the _motivation_ for your research question (with citations).

# Review Protocol

The _Review Protocol_ section documents the "_PICO_" (population, intervention, comparison, and  outcome)
elements for your review,  your _search string_, and your _inclusion_ and _exclusion_ criteria.

# Results

The _Results_ section documents the results of your search:

#. the number of papers identified by your search string in the initial search,
#. the number of papers that passed the "include title" step,
#. the number of papers that passed the "include abstract" step,
#. the number of papers that passed the "exlude" step (this should be the number of papers with with "Yes" in the "accept contents" column).
     
# Synthesis

The _Synthesis_ section for your report comprises three tables

#. a table listing all _codes_ used in the coding step, with an example quotation for each;
#. a table of _themes_, with the codes belonging to each theme.
#. a table of _model themes_, with *all* quotations that are grouped under each theme, and proper  citations to the source of the quotation.
 
If you don't know what these terms mean, review the synthesis tutorial
(recordings and  notes are on Canvas).

# Conclusion

The  _Conclusion_ section  comprises a paragraph that states the
one or two big things you learned from reading all these papers,
with proper citations to the supporting literature.


# References


The _References_ section contains _two_ lists of references:
 
1. A list of properly formatted references cited as supporting
evidence, mostly in the Introduction but possibly also in the
Synthesis and Conclusion.
2. A list of properly formatted references for *all* papers
accepted for review.


References should be in Harvard format.



